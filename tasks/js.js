var gulp = require('gulp');
var gulpIf = require('gulp-if');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var utils = require('./../utils/utils');
var debug = require('gulp-debug');
var jshint = require('gulp-jshint');
var jshintStylish = require('jshint-stylish');
var cached = require('gulp-cached');
var crypto = require('crypto');
var concat = require('gulp-concat');
var args = require('yargs').argv;
var extend = require('extend');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var storage = require('../utils/data-storage');
// var flatten = require('gulp-flatten');

module.exports = (function () {

    "use strict";

    var exports = {};

    var defaults = {
        createHintDev: {
            cached: {
                optimizeMemory: true
            }
        },
        createConcatDev: {
            sourcemaps: {
                init: {},
                write: {}
            },
            concat: {},
            notify: false
        },
        createHintProd: {},
        createConcatProd: {}
    };

    //error notification system for jshint
    var notifyHintError = {
        title: 'JS Hint Error', message: function (file) {
            if (file.jshint.success) {
                return false;
            }
            var errors = file.jshint.results.map(function (data) {
                if (data.error) {
                    return "( " + data.error.line + ':' + data.error.character + " ) " + data.error.reason;
                }
            }).join("\n");
            return file.relative + " ( " + file.jshint.results.length + " errors )\n" + errors;
        }
    };

    exports.createConcatDev = function (data, browserSync) {


        var fn = function concatJsDev() {

            browserSync = utils.checkBrowserSync(browserSync);

            var opts = extend(true, {}, defaults.createConcatDev, data);

            return gulp.src(opts.src)
                .pipe(gulpIf(Boolean(storage.getArg('debug')) || utils.debug, debug({ title: opts.debug || 'debug - ' })))
                .pipe(sourcemaps.init(opts.sourcemaps.init))
                .pipe(concat(opts.concat))
                .pipe(sourcemaps.write(opts.sourcemaps.write))
                .pipe(rename({
                    suffix: '.min',
                    extension: '.js'
                }))
                .pipe(gulp.dest(opts.dest))
                .pipe(gulpIf(Boolean(opts.notify.title) && utils.notify, notify(extend(true, {}, opts.notify, { onLast: true }))))
                .pipe(gulpIf(Boolean(browserSync), browserSync.stream({ once: true })));
        };

        return fn;
    };

    exports.createConcatProd = function (data, browserSync) {

        var fn = function concatJsProd() {

            var opts = extend(true, {}, defaults.createConcatProd, data);

            return gulp.src(opts.src)
                .pipe(gulpIf(Boolean(storage.getArg('debug')) || utils.debug, debug({ title: opts.debug || 'debug - ' })))
                .pipe(concat(opts.concat))
                .pipe(gulp.dest(opts.dest))
                .pipe(uglify({
                    compress: {
                        drop_console: true,
                        drop_debugger: true
                    }
                }))
                .pipe(rename({
                    suffix: '.min',
                    extension: '.js'
                }))
                .pipe(gulp.dest(opts.dest));
        };
        return fn;
    };

    exports.createHintProd = function (data) {


        var fn = function hindJsProd() {

            var opts = extend(true, {}, defaults.createHintProd, data);
            notify.logLevel(0);//disable loggin on the command line

            return gulp.src(opts.src)
                .pipe(gulpIf(Boolean(storage.getArg('debug')) || utils.debug, debug({ title: opts.debug || 'debug - ' })))
                .pipe(jshint(opts.jshint.file, opts.jshint))
                .pipe(jshint.reporter(jshintStylish))
                .pipe(notify(notifyHintError))
                .pipe(jshint.reporter('fail')); // fail production build if there are errors
        };

        return fn;

    };
    exports.createHintDev = function createJsHintDev(data) {


        var fn = function hintJsDev() {


            var opts = extend(true, {}, defaults.createHintDev, data);

            var cacheName = crypto.createHash('md5')
                .update(opts.src.toString(), 'utf-8')
                .digest('hex');


            notify.logLevel(0);//disable logging on the command line

            return gulp.src(opts.src)
                .pipe(gulpIf(Boolean(storage.getArg('debug')) || opts.debug, debug({ title: opts.debug || 'debug - ' })))
                .pipe(cached(cacheName, opts.cached))
                .pipe(jshint(opts.jshint.file, opts.jshint))
                .pipe(jshint.reporter(jshintStylish))
                .pipe(notify(notifyHintError))
        };

        return fn;
    };

    return exports;
})();
