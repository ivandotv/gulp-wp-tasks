var gulp = require('gulp');
var gulpIf = require('gulp-if');
var notify = require("gulp-notify");
var utils = require('./../utils/utils');
var debug = require('gulp-debug');
var cached = require('gulp-cached');
var storage = require('../utils/data-storage');
var wpPot = require('gulp-wp-pot');
var zip = require('gulp-zip');
var filter = require('gulp-filter');
var replace = require('gulp-replace');
var through2 = require('through2');
var del = require('del');
var extend = require('extend');
var exec = require('child_process').exec;
var fs = require('fs');
// var chown = require('gulp-chown');
module.exports = (function () {

	"use strict";

	var exports = {};

	exports.createPotFile = function (data) {


		var fn = function createPotFile() {
			console.log('create POT file');
			console.dir(data.wpPot);
			console.log('destination: ', data.dest);
			return gulp.src(data.src)
				.pipe(gulpIf(Boolean(storage.getArg('debug')) || data.debug, debug({ title: data.debug || 'debug -' })))
				.pipe(wpPot(data.wpPot))
				.pipe(gulp.dest(data.dest));
		};

		return fn;
	};

	exports.productionFiles = function (data) {


		var fn = function productionFiles() {

			var opts = extend(true, {}, data);
			var hasDevCodeRemove = false;
			var devCodeRemove = { restore: through2.obj };
			var hasDemoCodeRemove = false;
			var demoCodeRemove = { restore: through2.obj };


			if (opts.devCodeRemove) {
				hasDevCodeRemove = true;
				devCodeRemove = filter(opts.devCodeRemove, { restore: true });
			}
			if (opts.demoCodeRemove) {
				hasDemoCodeRemove = true;
				demoCodeRemove = filter(opts.demoCodeRemove, { restore: true });
			}
			//TODO - add npm chack
			console.log('production files:');
			console.log('remove DEV code: ', hasDevCodeRemove);
			console.log('remove DEMO code: ', hasDemoCodeRemove)
			console.log('destination: ', opts.dest);

			return gulp.src(opts.src)
				.pipe(gulpIf(Boolean(storage.getArg('debug')) || opts.debug, debug({ title: opts.debug || 'debug -' })))
				.pipe(gulpIf(hasDevCodeRemove, devCodeRemove))
				.pipe(gulpIf(hasDevCodeRemove, replace(/^\/\/==dev-code[\s\S]*?^\/\/==end-dev-code/gm, '', { skipBinary: true })))
				.pipe(gulpIf(hasDevCodeRemove, devCodeRemove.restore))
				.pipe(gulpIf(hasDemoCodeRemove, demoCodeRemove))
				.pipe(gulpIf(hasDemoCodeRemove, replace(/^\/\/==demo-code[\s\S]*?^\/\/==end-demo-code/gm, '', { skipBinary: true })))
				.pipe(gulpIf(hasDemoCodeRemove, demoCodeRemove.restore))
				// .pipe(chown(data.chown.user, data.chown.group))
				// .pipe(chown('1000', '33'))
				.pipe(gulp.dest(opts.dest));
		};

		return fn;
	};
	exports.changeOwnership = function (data) {

		return function () {
			return new Promise(function (resolve, reject) {

				if ('win32' !== process.platform) {
					data.user = data.user || '';

					console.log('change ownership:');
					console.log('user: ', data.user);
					console.log('group: ', data.group);
					console.log('path :', data.path);

					exec(`sudo chown -R ${data.user}:${data.group} ${data.path}`, function (err, stdout, stderr) {
						if (err) {
							reject(err);
						} else {
							resolve(stdout);
						}
					});
				}
				else {
					resolve();
				}
			});
		};

	};
	exports.clean = function (data) {

		var fn = function clean(done) {

			console.log('clean: ', data.src);

			del(data.src, { force: true })
				.then(function (paths) {
					done();
				});
		};
		return fn;
	};

	exports.zip = function (data) {

		var fn = function createZip() {

			console.log('create zip ');
			console.log('name: ', data.archiveName);
			console.log('destination ', data.dest);

			return gulp.src(data.src)
				.pipe(gulpIf(Boolean(storage.getArg('debug')) || data.debug, debug({ title: data.debug || 'debug -' })))
				.pipe(zip(data.archiveName))
				.pipe(gulp.dest(data.dest));
		};

		return fn;
	};


	exports.debugSrc = function testSrc(src) {
		return function debugSrc() {
			return gulp.src(src)
				.pipe(debug());
		};
	};

	return exports;

})();
