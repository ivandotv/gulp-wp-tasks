var utils = require('./../utils/utils');
var gulp = require('gulp');
var gulpIf = require('gulp-if');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var postCss = require('gulp-postcss');
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var debug = require('gulp-debug');
var cached = require('gulp-cached');
var extend = require('extend');
var storage = require('../utils/data-storage');
var rename = require('gulp-rename');
var through2 = require('through2');
var flatten = require('gulp-flatten');

module.exports = (function () {

	"use strict";

	var exports = {};

	var autoPrefixerOptions = {
		browsers: ['last 2 versions'],
		cascade: true
	};

	var cssNanoOptions = {
		reduceIdents: false,
		discardUnused: false
	}

	var defaults = {
		createConcatDev: {}
		// createSassDev: {
		// 	sourcemaps: {
		// 		init: {},
		// 		write: {}
		// 	},
		// 	notify: false
		// },
		// createSassProd: {
		// 	notify: false
		// }

	};

	function onSassError(err) {

		utils.log(err.message);
		notifier.notify({
			title: 'Gulp Sass Error',
			message: err.message
		});

		this.emit('end');
	}

	exports.createDev = function (data, browserSync) {


		var fn = function createCssDev() {

			browserSync = utils.checkBrowserSync(browserSync);

			var opts = extend(true, {}, defaults.createConcatDev, data);
			var postCssPlugins = [
				autoprefixer(opts.autoPrefixer || autoPrefixerOptions)
			];

			opts.notify = opts.notify || {};

			return gulp.src(opts.src)
				.pipe(gulpIf(Boolean(storage.getArg('debug')) || opts.debug, debug({
					title: opts.debug || 'debug - '
				})))
				// .pipe(cached(cacheName,  opts.cached))
				.pipe(sourcemaps.init())
				.pipe(sass(opts.sass))
				.on("error", onSassError)
				.pipe(postCss(postCssPlugins))
				.pipe(sourcemaps.write())
				.pipe(rename({
					suffix: '.min',
					extension: '.css'
				}))
				.pipe(gulpIf(opts.flatten, flatten()))
				// .pipe(gulpIf(opts.rename,rename(opts.rename)))
				.pipe(gulp.dest(opts.dest))
				.pipe(gulpIf(Boolean(opts.notify.title) && utils.notify, notify(extend(true, {}, opts.notify, {
					onLast: true
				}))))
				.pipe(gulpIf(Boolean(browserSync), browserSync.stream()));

		};

		return fn;

	};

	exports.createProd = function (data, browserSync) {

		var fn = function creatCssProd() {

			browserSync = utils.checkBrowserSync(browserSync);

			var opts = extend(true, {}, data);
			var postCssAutoprefixer = [
				autoprefixer(opts.autoPrefixer || autoPrefixerOptions)
			];

			var postCssNano = [cssnano(opts.cssNano || cssNanoOptions)];

			opts.notify = opts.notify || {};

			return gulp.src(opts.src)
				.pipe(gulpIf(Boolean(storage.getArg('debug')) || opts.debug, debug({
					title: opts.debug || 'debug - '
				})))
				.pipe(sass(opts.sass))
				.on("error", onSassError)
				.pipe(postCss(postCssAutoprefixer))
				.pipe(gulpIf(opts.flatten, flatten()))
				.pipe(gulp.dest(opts.dest))
				.pipe(postCss(postCssNano))
				.pipe(rename({
					suffix: '.min',
					extension: '.css'
				}))
				.pipe(gulpIf(opts.flatten, flatten()))
				.pipe(gulp.dest(opts.dest))
				.pipe(gulpIf(Boolean(opts.notify.title) && utils.notify, notify(extend(true, {}, opts.notify, {
					onLast: true
				}))))
				.pipe(gulpIf(Boolean(browserSync), browserSync.stream()));

		};

		return fn;

	};


	return exports;

})();