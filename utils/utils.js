"use strict";
var util = require('gulp-util');
var through2 = require('through2');
var extend = require('extend');
var storage = require('./data-storage');

exports.log = function log(msg) {

    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                util.log(util.colors.blue(msg[item]));
            }
        }
    } else {
        util.log(util.colors.blue(msg));
    }
};

exports.checkBrowserSync = function (browserSync) {

    if (!browserSync) {

        browserSync = {
            stream: function () {
                return through2.obj();
            }
        };

    }
    return browserSync;
};
//deprecated
exports.mergeOptions = function (pluginDefaults, data, key) {

    var opts = (pluginDefaults) ? pluginDefaults : {};
    key = (key) ? key : '';

    opts = extend(true, {}, opts, data.default);

    if ('prod' === key) {
        if (data.prod) {
            opts = extend(true, {}, opts, data.prod);
        }
    } else if ('dev' === key) {

        if (data.dev) {
            opts = extend(true, {}, opts, data.dev);
        }
    }
    return opts;
};


exports.notify = function useNotify() {

    return !storage.hasArg('quiet');
};

exports.debug = function useDebug() {
    return storage.hasArg('debug');
};
