"use strict";
var args = require('yargs').argv;
var extend = require('extend');

module.exports = (function () {


    function Storage() {

        this.args = extend(true, {}, args);

    }

    Storage.prototype.setArg = function (arg, value) {
        value = (typeof value === 'undefined') ? null : value;
        this.args[arg] = value;
    };

    Storage.prototype.getArg = function (arg) {

        return this.args[arg];
    };

    Storage.prototype.removeArg = function (arg) {
        if (this.hasArg(arg)) {
            delete this.args[arg];
        }
    };
    Storage.prototype.hasArg = function (arg) {

        return (typeof this.args[arg] !== 'undefined');

    };

    Storage.prototype.getAllArgs = function () {
        return this.args;
    };

    return new Storage();
})();
